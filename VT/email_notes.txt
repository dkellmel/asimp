09/15/2020
Hi Don,

Ok, great. I think we're at a point where porting code to ASI becomes possible.

The python temporal smoothing function will look something like the
attached txt document. I think the minimum amount of inputs will be:
- the 6xN received signal vector
- the channel coherence time
- the number of blocks to average over
- the window length of each block

I think the coherence time could be estimated from the signal vector but I'm
not sure on that. Also, if we want to use the full signal vector, then only
one of (blocks, window) needs to be given - the other can be calculated.

I think I'll get the python code to you by the end of the week, then we can
decide how to best modify it for use at ASI.

So, the temporal smoothing technique will not by itself need a dense manifold.
However to generate a good ML dataset (and validate it using MUSIC, which we
have been doing) we would need either a pretty dense manifold or a good way
to estimate it.

Yes, once you are getting good IQ data you could simply pass it through the TS
code and send us the feature vectors.

I'm attaching a paper that gives a good treatment of temporal smoothing,
this is pretty much what we used. I think it derives the equations for
the ULA but the vector sensor implementation doesn't change.

Will
----------------------------------------------------------------------------------------------------